const path = require("path");
const fs = require("fs");
const { runPythonScript, hashJSON } = require("../utils");

const renderDocument = async (data, tplPath) => {
  const hash = hashJSON([data, tplPath]);
  const docPath = path.resolve(__dirname, "../../public", `${hash}.docx`);
  if (!fs.existsSync(docPath)) {
    const jsonPath = path.resolve(__dirname, "../../public", `${hash}.json`);
    const json = JSON.stringify(data, null, 2);
    await fs.writeFileAsync(jsonPath, json);
    const script = path.resolve(__dirname, "main.py");
    await runPythonScript(script, [jsonPath, tplPath, docPath]);
  }
  return docPath;
};

module.exports = renderDocument;
