from sys import argv
import json
from docxtpl import DocxTemplate, InlineImage
from docx.shared import Mm
import jinja2


def multiply_by(value, by):
    return value * by


def renderDocument(json_path, tpl_path, doc_path):
    jinja_env = jinja2.Environment(autoescape=True)
    jinja_env.filters['multiply_by'] = multiply_by
    doc = DocxTemplate(tpl_path)

    with open(json_path) as f:
        context = json.load(f)
        doc.render(context, jinja_env)
        doc.save(doc_path)


if __name__ == '__main__':
    json_path = argv[1]
    tpl_path = argv[2]
    doc_path = argv[3]
    renderDocument(json_path, tpl_path, doc_path)
