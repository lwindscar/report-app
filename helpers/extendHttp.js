const path = require("path");
const { merge } = require("lodash");
const onFinished = require("on-finished");
const contentType = require("content-type");
const send = require("send");
const contentDisposition = require("content-disposition");
const cookie = require("cookie");
const { sign } = require("cookie-signature");

const { mime } = send;

/**
 * Stringify JSON, like JSON.stringify, but v8 optimized, with the
 * ability to escape characters that can trigger HTML sniffing.
 *
 * @param {*} value
 * @param {function} replaces
 * @param {number} spaces
 * @param {boolean} escape
 * @returns {string}
 * @private
 **/

function stringify(value, replacer, spaces, escape) {
  // v8 checks arguments.length for optimizing simple call
  // https://bugs.chromium.org/p/v8/issues/detail?id=4730
  let json =
    replacer || spaces
      ? JSON.stringify(value, replacer, spaces)
      : JSON.stringify(value);

  if (escape) {
    json = json.replace(/[<>&]/g, function(c) {
      switch (c.charCodeAt(0)) {
        case 0x3c:
          return "\\u003c";
        case 0x3e:
          return "\\u003e";
        case 0x26:
          return "\\u0026";
        /* istanbul ignore next: unreachable default */
        default:
          return c;
      }
    });
  }

  return json;
}

// pipe the send file stream
function sendfile(res, file, options, callback) {
  let done = false;
  let streaming;

  // request aborted
  function onaborted() {
    if (done) return;
    done = true;

    const err = new Error("Request aborted");
    err.code = "ECONNABORTED";
    callback(err);
  }

  // directory
  function ondirectory() {
    if (done) return;
    done = true;

    const err = new Error("EISDIR, read");
    err.code = "EISDIR";
    callback(err);
  }

  // errors
  function onerror(err) {
    if (done) return;
    done = true;
    callback(err);
  }

  // ended
  function onend() {
    if (done) return;
    done = true;
    callback();
  }

  // file
  function onfile() {
    streaming = false;
  }

  // finished
  function onfinish(err) {
    if (err && err.code === "ECONNRESET") return onaborted();
    if (err) return onerror(err);
    if (done) return;

    setImmediate(function() {
      if (streaming !== false && !done) {
        onaborted();
        return;
      }

      if (done) return;
      done = true;
      callback();
    });
  }

  // streaming
  function onstream() {
    streaming = true;
  }

  file.on("directory", ondirectory);
  file.on("end", onend);
  file.on("error", onerror);
  file.on("file", onfile);
  file.on("stream", onstream);
  onFinished(res, onfinish);

  if (options.headers) {
    // set headers on successful transfer
    file.on("headers", function headers(res) {
      const obj = options.headers;
      const keys = Object.keys(obj);

      for (const i = 0; i < keys.length; i++) {
        const k = keys[i];
        res.setHeader(k, obj[k]);
      }
    });
  }

  // pipe
  file.pipe(res);
}

function setCharset(type, charset) {
  if (!type || !charset) {
    return type;
  }

  // parse type
  const parsed = contentType.parse(type);

  // set charset
  parsed.parameters.charset = charset;

  // format type
  return contentType.format(parsed);
}

function isAbsolute(path) {
  if ("/" === path[0]) return true;
  if (":" === path[1] && ("\\" === path[2] || "/" === path[2])) return true; // Windows device path
  if ("\\\\" === path.substring(0, 2)) return true; // Microsoft Azure absolute path
}

module.exports = function extendHttp(settings = {}) {
  const { jsonpCallbackName = "callback", cookieSecret } = settings;

  return function(req, res, next) {
    /* Extend Request */
    req.get = req.header = function header(name) {
      if (!name) {
        throw new TypeError("name argument is required to req.get");
      }

      if (typeof name !== "string") {
        throw new TypeError("name must be a string to req.get");
      }

      const lc = name.toLowerCase();

      switch (lc) {
        case "referer":
        case "referrer":
          return req.getHeader("referrer") || req.getHeader("referer");
        default:
          return req.getHeader(lc);
      }
    };

    /* Extend Response */

    /**
     * Set status `code`.
     *
     * @param {Number} code
     * @return {ServerResponse}
     * @public
     */

    res.status = function status(code) {
      res.statusCode = code;
      return res;
    };

    /**
     * Send a response.
     *
     * Examples:
     *
     *     res.send(Buffer.from('wahoo'));
     *     res.send({ some: 'json' });
     *     res.send('<p>some html</p>');
     *
     * @param {string|number|boolean|object|Buffer} body
     * @public
     */

    res.send = function send(body) {
      let chunk = body;
      let encoding;
      let type;

      switch (typeof chunk) {
        // string defaulting to html
        case "string":
          if (!res.get("Content-Type")) {
            res.type("html");
          }
          break;
        case "boolean":
        case "number":
        case "object":
          if (chunk === null) {
            chunk = "";
          } else if (Buffer.isBuffer(chunk)) {
            if (!res.get("Content-Type")) {
              res.type("bin");
            }
          } else {
            return res.json(chunk);
          }
          break;
      }

      // write strings in utf-8
      if (typeof chunk === "string") {
        encoding = "utf8";
        type = res.get("Content-Type");

        // reflect this in content-type
        if (typeof type === "string") {
          res.set("Content-Type", setCharset(type, "utf-8"));
        }
      }

      // populate Content-Length
      let len;
      if (chunk !== undefined) {
        if (Buffer.isBuffer(chunk)) {
          // get length of Buffer
          len = chunk.length;
        } else {
          // convert chunk to Buffer and calculate
          chunk = Buffer.from(chunk, encoding);
          encoding = undefined;
          len = chunk.length;
        }

        res.set("Content-Length", len);
      }

      // strip irrelevant headers
      if (204 === res.statusCode || 304 === res.statusCode) {
        res.removeHeader("Content-Type");
        res.removeHeader("Content-Length");
        res.removeHeader("Transfer-Encoding");
        chunk = "";
      }

      if (req.method === "HEAD") {
        // skip body for HEAD
        res.end();
      } else {
        // respond
        res.end(chunk, encoding);
      }

      return res;
    };

    /**
     * Send JSON response.
     *
     * Examples:
     *
     *     res.json(null);
     *     res.json({ user: 'tj' });
     *
     * @param {string|number|boolean|object} obj
     * @public
     */

    res.json = function json(obj) {
      const val = obj;

      // settings
      const body = stringify(val, null, 2);

      // content-type
      if (!res.get("Content-Type")) {
        res.set("Content-Type", "application/json");
      }

      return res.send(body);
    };

    /**
     * Send JSON response with JSONP callback support.
     *
     * Examples:
     *
     *     res.jsonp(null);
     *     res.jsonp({ user: 'tj' });
     *
     * @param {string|number|boolean|object} obj
     * @public
     */

    res.jsonp = function jsonp(obj) {
      const val = obj;

      // settings
      let body = stringify(val, null, 2);
      let callback = req.query[jsonpCallbackName];

      // content-type
      if (!res.get("Content-Type")) {
        res.set("X-Content-Type-Options", "nosniff");
        res.set("Content-Type", "application/json");
      }

      // fixup callback
      if (Array.isArray(callback)) {
        callback = callback[0];
      }

      // jsonp
      if (typeof callback === "string" && callback.length !== 0) {
        res.set("X-Content-Type-Options", "nosniff");
        res.set("Content-Type", "text/javascript");

        // restrict callback charset
        callback = callback.replace(/[^[\]\w$.]/g, "");

        // replace chars not allowed in JavaScript that are in JSON
        body = body.replace(/\u2028/g, "\\u2028").replace(/\u2029/g, "\\u2029");

        // the /**/ is a specific security mitigation for "Rosetta Flash JSONP abuse"
        // the typeof check is just to reduce client error noise
        body =
          "/**/ typeof " +
          callback +
          " === 'function' && " +
          callback +
          "(" +
          body +
          ");";
      }

      return res.send(body);
    };

    /**
     * Transfer the file at the given `path`.
     *
     * Automatically sets the _Content-Type_ response header field.
     * The callback `callback(err)` is invoked when the transfer is complete
     * or when an error occurs. Be sure to check `res.sentHeader`
     * if you wish to attempt responding, as the header and some data
     * may have already been transferred.
     *
     * Options:
     *
     *   - `maxAge`   defaulting to 0 (can be string converted by `ms`)
     *   - `root`     root directory for relative filenames
     *   - `headers`  object of headers to serve with file
     *   - `dotfiles` serve dotfiles, defaulting to false; can be `"allow"` to send them
     *
     * Other options are passed along to `send`.
     *
     * Examples:
     *
     *  The following example illustrates how `res.sendFile()` may
     *  be used as an alternative for the `static()` middleware for
     *  dynamic situations. The code backing `res.sendFile()` is actually
     *  the same code, so HTTP cache support etc is identical.
     *
     *     app.get('/user/:uid/photos/:file', function(req, res){
     *       const uid = req.params.uid
     *         , file = req.params.file;
     *
     *       req.user.mayViewFilesFrom(uid, function(yes){
     *         if (yes) {
     *           res.sendFile('/uploads/' + uid + '/' + file);
     *         } else {
     *           res.send(403, 'Sorry! you cant see that.');
     *         }
     *       });
     *     });
     *
     * @public
     */

    res.sendFile = function sendFile(path, options, callback) {
      let done = callback;
      let opts = options || {};

      if (!path) {
        throw new TypeError("path argument is required to res.sendFile");
      }

      if (typeof path !== "string") {
        throw new TypeError("path must be a string to res.sendFile");
      }

      // support function as second arg
      if (typeof options === "function") {
        done = options;
        opts = {};
      }

      if (!opts.root && !isAbsolute(path)) {
        throw new TypeError(
          "path must be absolute or specify root to res.sendFile"
        );
      }

      // create file stream
      const pathname = encodeURI(path);
      const file = send(req, pathname, opts);

      // transfer
      sendfile(res, file, opts, function(err) {
        if (done) return done(err);
        if (err && err.code === "EISDIR") return next();

        // next() all but write errors
        if (err && err.code !== "ECONNABORTED" && err.syscall !== "write") {
          next(err);
        }
      });
    };

    /**
     * Transfer the file at the given `path` as an attachment.
     *
     * Optionally providing an alternate attachment `filename`,
     * and optional callback `callback(err)`. The callback is invoked
     * when the data transfer is complete, or when an error has
     * ocurred. Be sure to check `res.headersSent` if you plan to respond.
     *
     * Optionally providing an `options` object to use with `res.sendFile()`.
     * This function will set the `Content-Disposition` header, overriding
     * any `Content-Disposition` header passed as header options in order
     * to set the attachment and filename.
     *
     * This method uses `res.sendFile()`.
     *
     * @public
     */

    res.download = function download(path, filename, options, callback) {
      let done = callback;
      let name = filename;
      let opts = options || null;

      // support function as second or third arg
      if (typeof filename === "function") {
        done = filename;
        name = null;
        opts = null;
      } else if (typeof options === "function") {
        done = options;
        opts = null;
      }

      // set Content-Disposition when file is sent
      const headers = {
        "Content-Disposition": contentDisposition(name || path)
      };

      // merge user-provided headers
      if (opts && opts.headers) {
        const keys = Object.keys(opts.headers);
        for (let i = 0; i < keys.length; i++) {
          const key = keys[i];
          if (key.toLowerCase() !== "content-disposition") {
            headers[key] = opts.headers[key];
          }
        }
      }

      // merge user-provided options
      opts = Object.create(opts);
      opts.headers = headers;

      // Resolve the full path for sendFile
      const fullPath = path.resolve(path);

      // send file
      return res.sendFile(fullPath, opts, done);
    };

    /**
     * Set _Content-Type_ response header with `type` through `mime.lookup()`
     * when it does not contain "/", or set the Content-Type to `type` otherwise.
     *
     * Examples:
     *
     *     res.type('.html');
     *     res.type('html');
     *     res.type('json');
     *     res.type('application/json');
     *     res.type('png');
     *
     * @param {String} type
     * @return {ServerResponse} for chaining
     * @public
     */

    res.contentType = res.type = function contentType(type) {
      const ct = type.indexOf("/") === -1 ? mime.lookup(type) : type;

      return res.set("Content-Type", ct);
    };

    /**
     * Set _Content-Disposition_ header to _attachment_ with optional `filename`.
     *
     * @param {String} filename
     * @return {ServerResponse}
     * @public
     */

    res.attachment = function attachment(filename) {
      if (filename) {
        res.type(path.extname(filename));
      }

      res.set("Content-Disposition", contentDisposition(filename));

      return res;
    };

    /**
     * Append additional header `field` with value `val`.
     *
     * Example:
     *
     *    res.append('Link', ['<http://localhost/>', '<http://localhost:3000/>']);
     *    res.append('Set-Cookie', 'foo=bar; Path=/; HttpOnly');
     *    res.append('Warning', '199 Miscellaneous warning');
     *
     * @param {String} field
     * @param {String|Array} val
     * @return {ServerResponse} for chaining
     * @public
     */

    res.append = function append(field, val) {
      const prev = res.get(field);
      let value = val;

      if (prev) {
        // concat the new and prev vals
        if (Array.isArray(prev)) {
          value = prev.concat(val);
        } else if (Array.isArray(val)) {
          value = [prev].concat(val);
        } else {
          value = [prev, val];
        }
      }

      return res.set(field, value);
    };

    /**
     * Set header `field` to `val`, or pass
     * an object of header fields.
     *
     * Examples:
     *
     *    res.set('Foo', ['bar', 'baz']);
     *    res.set('Accept', 'application/json');
     *    res.set({ Accept: 'text/plain', 'X-API-Key': 'tobi' });
     *
     * Aliased as `res.header()`.
     *
     * @param {String|Object} field
     * @param {String|Array} val
     * @return {ServerResponse} for chaining
     * @public
     */

    res.set = res.header = function header(field, val) {
      if (arguments.length === 2) {
        const charsetRegExp = /;\s*charset\s*=/;
        let value = Array.isArray(val) ? val.map(String) : String(val);

        // add charset to content-type
        if (field.toLowerCase() === "content-type") {
          if (Array.isArray(value)) {
            throw new TypeError("Content-Type cannot be set to an Array");
          }
          if (!charsetRegExp.test(value)) {
            const charset = mime.charsets.lookup(value.split(";")[0]);
            if (charset) value += "; charset=" + charset.toLowerCase();
          }
        }

        res.setHeader(field, value);
      } else {
        for (const key in field) {
          res.set(key, field[key]);
        }
      }
      return res;
    };

    /**
     * Get value for header `field`.
     *
     * @param {String} field
     * @return {String}
     * @public
     */

    res.get = function(field) {
      return res.getHeader(field);
    };

    /**
     * Clear cookie `name`.
     *
     * @param {String} name
     * @param {Object} [options]
     * @return {ServerResponse} for chaining
     * @public
     */

    res.clearCookie = function clearCookie(name, options) {
      const opts = merge({ expires: new Date(1), path: "/" }, options);

      return res.cookie(name, "", opts);
    };

    /**
     * Set cookie `name` to `value`, with the given `options`.
     *
     * Options:
     *
     *    - `maxAge`   max-age in milliseconds, converted to `expires`
     *    - `signed`   sign the cookie
     *    - `path`     defaults to "/"
     *
     * Examples:
     *
     *    // "Remember Me" for 15 minutes
     *    res.cookie('rememberme', '1', { expires: new Date(Date.now() + 900000), httpOnly: true });
     *
     *    // same as above
     *    res.cookie('rememberme', '1', { maxAge: 900000, httpOnly: true })
     *
     * @param {String} name
     * @param {String|Object} value
     * @param {Object} [options]
     * @return {ServerResponse} for chaining
     * @public
     */

    res.cookie = function(name, value, options) {
      const opts = merge({}, options);
      const secret = cookieSecret;
      const signed = opts.signed;

      if (signed && !secret) {
        throw new Error("cookieParser('secret') required for signed cookies");
      }

      let val =
        typeof value === "object"
          ? "j:" + JSON.stringify(value)
          : String(value);

      if (signed) {
        val = "s:" + sign(val, secret);
      }

      if ("maxAge" in opts) {
        opts.expires = new Date(Date.now() + opts.maxAge);
        opts.maxAge /= 1000;
      }

      if (opts.path == null) {
        opts.path = "/";
      }

      res.append("Set-Cookie", cookie.serialize(name, String(val), opts));

      return res;
    };

    next();
  };
};
