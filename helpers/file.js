const path = require("path");
const crypto = require("crypto");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: "public/",
  filename: function(req, file, cb) {
    crypto.randomBytes(16, (err, buf) => {
      if (err) {
        cb(err);
      } else {
        const hex = buf.toString("hex");
        const extname = path.extname(file.originalname);
        cb(null, `${hex}${extname}`);
      }
    });
  }
});

const upload = multer({ storage });

module.exports = upload;
