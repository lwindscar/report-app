const { spawn } = require("child_process");
const crypto = require("crypto");
const _ = require("lodash");

const delay = (ms = 0, payload) =>
  new Promise(resolve => setTimeout(() => resolve(payload), ms));

const uint8arrayToString = data => String.fromCharCode.apply(null, data);

const runPythonScript = (script, args) => {
  const command = "pipenv";
  const executionArgs = ["run", "python", "-u", script, ...args];
  return new Promise((resolve, reject) => {
    const scriptExecution = spawn(command, executionArgs);
    let error = "";
    let response = "";

    // Handle normal output
    scriptExecution.stdout.on("data", data => {
      response += uint8arrayToString(data);
      response += "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
    });

    // Handle error output
    scriptExecution.stderr.on("data", data => {
      error += uint8arrayToString(data);
      error += "\n******************************************************\n";
    });

    scriptExecution.on("error", err => {
      reject(err, 333);
    });

    scriptExecution.on("exit", code => {
      if (code === 0 && error === "") {
        resolve(response);
      } else {
        reject(error, code);
      }
    });
  });
};

const hashJSON = json => {
  const data = _.isString(json) ? json : JSON.stringify(json);
  const hash = crypto.createHash("sha1");
  hash.update(data, "utf8");
  return hash.digest("hex");
};

module.exports = {
  delay,
  uint8arrayToString,
  runPythonScript,
  hashJSON
};
