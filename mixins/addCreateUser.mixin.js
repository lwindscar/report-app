module.exports = function() {
  return {
    hooks: {
      before: {
        create: [
          function(ctx) {
            const { user } = ctx.meta;
            ctx.params.createUser = user.id;
            return ctx;
          }
        ]
      }
    }
  };
};
