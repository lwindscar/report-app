const _ = require("lodash");
const { Errors } = require("moleculer");

const { MoleculerError } = Errors;

module.exports = function(field) {
  const prefix = `${field}CheckUnique`;
  const hookName = `${prefix}Hook`;
  const insertHookName = `${prefix}InsertHook`;
  return {
    methods: {
      [hookName]: async function(ctx) {
        const value = ctx.params[field];
        if (value) {
          const record = await this.adapter.findOne({ [field]: value });
          if (record) {
            throw new MoleculerError(`${field} ${value} is existed`, 400);
          }
        }
      },
      [insertHookName]: async function(ctx) {
        const { entity, entities } = ctx.params;
        if (entity) {
          const value = entity[field];
          if (value) {
            const record = await this.adapter.findOne({ [field]: value });
            if (record) {
              throw new MoleculerError(`${field} ${value} is existed`, 400);
            }
          }
        }
        if (entities) {
          const values = _.map(entities, `${field}`);
          const records = await this.adapter.find({
            $in: { [field]: values }
          });
          if (records.length > 0) {
            const existedValues = _.map(records, `${field}`);
            const text = _.join(existedValues, "|");
            throw new MoleculerError(`${field} ${text} is existed`, 400);
          }
        }
        return ctx;
      }
    },
    hooks: {
      before: {
        create: [hookName],
        update: [hookName],
        insert: [insertHookName]
      }
    }
  };
};
