const mongoose = require("mongoose");

const FileSchema = new mongoose.Schema(
  {
    name: {
      type: String
    },
    filename: {
      type: String,
      required: true
    },
    path: {
      type: String,
      required: true
    },
    mime: {
      type: String,
      required: true
    },
    size: {
      type: Number,
      required: true
    },
    extname: {
      type: String,
      required: true
    },
    createUser: {
      type: String,
      required: true,
      index: true
    }
  },
  {
    toJSON: {
      getters: true,
      versionKey: false
    },
    timestamps: true
  }
);

const FileModel = mongoose.model("File", FileSchema);

module.exports = FileModel;
