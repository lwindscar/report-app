const mongoose = require("mongoose");

const TemplateSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true
    },
    config: {
      type: String,
      required: true
    },
    createUser: {
      // type: mongoose.Schema.Types.ObjectId,
      type: String,
      required: true,
      index: true
    },
    file: {
      type: String
    }
  },
  {
    toJSON: {
      getters: true,
      versionKey: false
    },
    timestamps: true
  }
);

const TemplateModel = mongoose.model("Template", TemplateSchema);

module.exports = TemplateModel;
