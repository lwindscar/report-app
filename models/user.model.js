const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      index: true,
      require: true,
      unique: true
    },
    password: {
      type: String,
      require: true
    },
    name: {
      type: String,
      index: true
    },
    bio: String,
    avatar: String
  },
  {
    toJSON: {
      getters: true,
      versionKey: false,
      transform(doc, ret) {
        Reflect.deleteProperty(ret, "password");
        return ret;
      }
    },
    timestamps: true
  }
);

const UserModel = mongoose.model("User", UserSchema);

module.exports = UserModel;
