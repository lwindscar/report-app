"use strict";

const ApiGateway = require("moleculer-web");
const upload = require("../helpers/file");
const extendHttp = require("../helpers/extendHttp");

const {
  UnAuthorizedError,
  ERR_INVALID_TOKEN,
  ERR_NO_TOKEN
} = ApiGateway.Errors;

module.exports = {
  name: "api",
  mixins: [ApiGateway],
  settings: {
    port: process.env.PORT || 3333,
    path: "/api",
    use: [extendHttp()],
    routes: [
      {
        path: "/auth",
        mappingPolicy: "restrict",
        bodyParsers: {
          json: true
        },
        aliases: {
          "POST login": "auth.login"
        }
      },
      {
        path: "",
        mappingPolicy: "restrict",
        authorization: true,
        bodyParsers: {
          json: true
        },
        aliases: {
          "GET users": "users.list",
          "GET users/:id": "users.get",
          "POST users": "users.create",
          "PUT users/:id": "users.update",
          "DELETE users/:id": "users.remove",
          "REST templates": "templates",
          "REST files": "files",
          "POST files/upload": [
            upload.single("file"),
            function(req, res, next) {
              const { file } = req;
              req.$params = {
                name: file.originalname,
                filename: file.filename,
                path: file.path,
                mime: file.mimetype,
                size: file.size
              };
              next();
            },
            "files.create"
          ],
          "POST templates/createReport": "templates.createReport"
        }
      }
    ],

    // Serve assets from "public" folder
    assets: {
      folder: "public"
    }
  },

  actions: {},

  methods: {
    async authorize(ctx, route, req) {
      const auth = req.headers["authorization"];
      if (auth && auth.startsWith("Bearer")) {
        const token = auth.slice(7);
        const payload = await ctx.call("auth.authorize", { token });
        if (payload) {
          const user = await ctx.call("users.get", { id: payload.id });
          req.user = user; // eslint-disable-line
          ctx.meta.user = user; // eslint-disable-line
          return ctx;
        } else {
          throw new UnAuthorizedError(ERR_INVALID_TOKEN);
        }
      } else {
        throw new UnAuthorizedError(ERR_NO_TOKEN);
      }
    }
  }
};
