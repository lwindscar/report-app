const { Errors } = require("moleculer");
const jwt = require("jsonwebtoken");

module.exports = {
  name: "auth",
  settings: {
    tokenSecret: process.env.TOKEN_SECRET || "ymDgi6cHUnaR/8exnZu"
  },
  actions: {
    login: {
      params: {
        username: { type: "string", min: 3, max: 8 },
        password: { type: "string", min: 6, max: 20 }
      },
      async handler(ctx) {
        const user = await ctx.call("users.authenticate", ctx.params);
        if (!user) {
          throw new Errors.MoleculerError("用户名或密码错误", 401);
        }
        const token = this.generateToken(user);
        return { token };
      }
    },
    async authorize(ctx) {
      const { token } = ctx.params;
      const decoded = this.verifyToken(token);
      return decoded;
    }
  },
  methods: {
    generateToken(payload) {
      const token = jwt.sign(payload, this.settings.tokenSecret, {
        expiresIn: "1 days"
      });
      return token;
    },
    verifyToken(token) {
      try {
        const decoded = jwt.verify(token, this.settings.tokenSecret);
        return decoded;
      } catch (e) {
        this.logger.error(e);
        return null;
      }
    }
  }
};
