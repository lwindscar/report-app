const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const path = require("path");
const File = require("../models/file.model");

module.exports = {
  name: "files",
  mixins: [DbService],
  adapter: new MongooseAdapter(
    process.env.MONGO_URI || "mongodb://127.0.0.1:27017/report-app"
  ),
  model: File,
  settings: {
    idField: "id",
    populates: {
      createUser: "users.get"
    }
  },
  actions: {
    create: {
      // params: {
      //   name: { type: "string", trim: true },
      //   config: { type: "string" },
      //   createUser: { type: "string" },
      //   file: { type: "string", optional: true }
      // },
    }
  },
  methods: {},
  hooks: {
    before: {
      create: [
        function(ctx) {
          const { user } = ctx.meta;
          const { filename } = ctx.params;
          ctx.params.createUser = user.id;
          ctx.params.extname = path.extname(filename);
          return ctx;
        }
      ]
    }
  }
};
