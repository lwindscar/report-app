const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const path = require("path");
const Template = require("../models/template.model");
const checkUniqueMixin = require("../mixins/checkUnique.mixin");
const addCreateUserMixin = require("../mixins/addCreateUser.mixin");
const renderDocument = require("../helpers/docx-template");

module.exports = {
  name: "templates",
  mixins: [DbService, checkUniqueMixin("name"), addCreateUserMixin()],
  adapter: new MongooseAdapter(
    process.env.MONGO_URI || "mongodb://127.0.0.1:27017/report-app"
  ),
  model: Template,
  settings: {
    idField: "id",
    populates: {
      createUser: "users.get",
      file: "files.get"
    }
  },
  actions: {
    create: {
      params: {
        name: { type: "string", trim: true },
        config: { type: "string" },
        createUser: { type: "string" },
        file: { type: "string", optional: true }
      }
    },
    createReport: {
      params: {
        data: { type: "object" },
        template: { type: "string" }
      },
      async handler(ctx) {
        const { data, template } = ctx.params;
        const { file } = await ctx.call("templates.get", {
          id: template,
          populate: "file"
        });
        const tplName = file.filename;
        const tplPath = path.resolve(__dirname, "../public", tplName);
        const docPath = await renderDocument(data, tplPath);
        return docPath;
      }
    }
  },
  methods: {}
};
