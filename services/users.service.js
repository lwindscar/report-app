const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");
const bcrypt = require("bcryptjs");
const _ = require("lodash");
const User = require("../models/user.model");
const checkUniqueMixin = require("../mixins/checkUnique.mixin");

module.exports = {
  name: "users",
  mixins: [DbService, checkUniqueMixin("username")],
  adapter: new MongooseAdapter(
    process.env.MONGO_URI || "mongodb://127.0.0.1:27017/report-app"
  ),
  model: User,
  settings: {
    idField: "id"
  },
  actions: {
    create: {
      params: {
        username: { type: "string", min: 3, max: 10 },
        password: { type: "string", min: 6, max: 20 },
        // confirmPassword: { type: "equal", field: "password", strict: true },
        name: { type: "string", trim: true, min: 2, optional: true },
        bio: { type: "string", optional: true },
        avatar: { type: "string", optional: true }
      }
    },
    async authenticate(ctx) {
      const { username, password } = ctx.params;
      const user = await this.adapter.findOne({ username });
      if (user && this.comparePwd(password, user.password)) {
        return user.toJSON();
      }
      return null;
    }
  },
  methods: {
    hashPwd(password) {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(password, salt);
      return hash;
    },
    comparePwd(password, hash) {
      return bcrypt.compareSync(password, hash);
    },
    hashPwdHook(ctx) {
      const { password, entity, entities } = ctx.params;
      if (password) {
        ctx.params.password = this.hashPwd(password);
      }
      if (entity) {
        ctx.params.entity.password = this.hashPwd(entity.password);
      }
      if (entities) {
        ctx.params.entities = _.map(entities, item =>
          _.assign({}, item, { password: this.hashPwd(item.password) })
        );
      }
      return ctx;
    }
  },
  hooks: {
    before: {
      create: ["hashPwdHook"],
      update: ["hashPwdHook"],
      insert: ["hashPwdHook"]
    }
  }
};
